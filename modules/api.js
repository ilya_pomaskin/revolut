import { delay } from 'redux-saga/effects'
import { API_KEY } from '../config';

export function getCurrency() {
    const queryParams = new URLSearchParams();
    queryParams.set('app_id', API_KEY);
    queryParams.set('base', 'USD');
    queryParams.set('prettyprint', true);
    queryParams.set('symbols', 'EUR,USD,GBP');

    return fetch('https://openexchangerates.org/api/latest.json?' + queryParams.toString(), {
        method: 'GET',
    }).then(response => response.json());
}

export function* mockGetCurrency() {
    return yield delay(Math.random() * 100, {
        disclaimer: "Usage subject to terms: https://openexchangerates.org/terms",
        license: "https://openexchangerates.org/license",
        timestamp: Date.now(),
        base: 'USD',
        rates: {
            "EUR": 0.908913 + (Math.random() * 0.05),
            "GBP": 0.776434 + (Math.random() * 0.05),
            "USD": 1
        }
    });
}