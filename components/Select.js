import React from 'react';

export const Select = ({ value, onChange }) => (
    <div className="container">
        <style jsx>{`
            .select {
                height: 20px;
                width: calc(100% + 20px);
                appearance: none;
                font-size: 20px;
                line-height: 1;
                background: transparent;
                color: white;
                border: 0;
                outline: none;
            }
            .container {
                display: inline-block;
                line-height: 0;
                position: relative;
            }
            .container::after {
                content: ' ';
                pointer-events: none;
                position: absolute;
                left: 100%;
                top: 50%;
                transform: translateX(50%) translateY(-50%);
                width: 0; 
                height: 0; 
                border-left: 5px solid transparent;
                border-right: 5px solid transparent;
                border-top: 5px solid white;              
            }
        `}</style>

        <select
            className="select"
            value={value}
            onChange={onChange}
        >
            <option value="USD">USD</option>
            <option value="EUR">EUR</option>
            <option value="GBP">GBP</option>
        </select>
    </div>
);
