import React from 'react';

const Sizes = {
    BIG: 'big',
    SMALL: 'small'
};

const Colors = {
    WHITE: 'white',
    WHITE_OPACITY: 'white-opacity',
    RED: 'red'
};

export const Text = ({
    size = Sizes.SMALL,
    color = Colors.WHITE,
    children
}) => (
        <p className={`text ${size} ${color}`}>
            <style jsx>{`
                .big {
                    font-size: 20px;   
                }

                .small {
                    font-size: 12px;
                }

                .white {
                    color: white;
                }

                .white-opacity {
                    color: rgba(255, 255, 255, 0.6);
                }

                .red {
                    color: red;
                }
            `}</style>
            {children}
        </p>
    );

Text.Sizes = Sizes;
Text.Colors = Colors;