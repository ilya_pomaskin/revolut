import React from 'react';
import renderer from 'react-test-renderer';
import { Select } from '../Select';

describe('Select', () => {
    it('renders correctly', () => {
        expect(
            renderer
                .create(
                    <Select
                        value="USD"
                        onChange={() => null}
                    />
                )
                .toJSON()
        ).toMatchSnapshot();
    });
});