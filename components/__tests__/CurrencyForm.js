import React from 'react';
import renderer from 'react-test-renderer';
import { CurrencyForm } from '../CurrencyForm';

describe('CurrencyForm', () => {
    it('renders correctly', () => {
        expect(
            renderer
                .create(
                    <CurrencyForm
                        isSource
                        currency='USD'
                        sign='+'
                        amount={123}
                        rate={2.36}
                        pocketAmount={999}
                        onCurrencyChange={() => null}
                        onAmountChange={() => null}
                    />
                )
                .toJSON()
        ).toMatchSnapshot();
    });
});