import React from 'react';
import renderer from 'react-test-renderer';
import { Text } from '../Text';

describe('Text', () => {
    it('renders correctly', () => {
        expect(
            renderer
                .create(<Text
                    size={Text.Sizes.BIG}
                    color={Text.Colors.RED}
                >
                    child
            </Text>)
                .toJSON()
        ).toMatchSnapshot();
    });
});