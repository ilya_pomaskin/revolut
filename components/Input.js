import React from 'react';

export const Input = ({ sign, value, onChange }) => (
    <div className="input-container">
        <style jsx>{`
            .input-container {
                position: relative;
            }
            .input-container::before {
                position: absolute;
                top: 0;
                left: 0;
                transform: translateX(-100%);
                font-size: 20px;
                content: '${value > 0 ? sign : ''}'
            }
            .input {
                height: 20px;
                border: none;
                background: transparent;
                color: white;
                font-size: 20px;
                line-height: 1;
                text-align: left;
                padding: 0;
            }
            .input:focus {
                outline: none;
            }
            .input[type=number]::-webkit-inner-spin-button, 
            .input[type=number]::-webkit-outer-spin-button { 
                -webkit-appearance: none; 
                margin: 0; 
            }
            
        `}</style>

        <input
            className="input"
            type="number"
            value={String(value)}
            onChange={onChange}
        />
    </div>
);
