import React from 'react';

export const Footer = ({ children }) => (
    <div className="container">
        <style jsx>{`
            .container {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                padding: 20px;
                background: rgba(21,90,196,1);
                color: white;
            }
        `}</style>

        {children}
    </div>
);