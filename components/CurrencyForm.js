import React from 'react';

import { Text } from './Text';
import { Input } from './Input';
import { Select } from './Select';

const formatDecimals = n => Math.floor(n * 100) / 100;

export const CurrencyForm = ({
    isSource,
    currency,
    sign,
    amount,
    rate,
    pocketAmount,
    onCurrencyChange,
    onAmountChange
}) => (
        <div className="container">
            <style jsx>{`
                .container {
                    display: flex;
                    background: ${isSource
                    ? "radial-gradient(circle, rgba(48,120,249,1) 0%, rgba(14,83,206,1) 100%)"
                    : "radial-gradient(circle, rgba(21,90,196,1) 0%, rgba(20,90,193,1) 100%)"};
                    color: white;
                }
                .left-side,
                .right-side {
                    flex: 1;
                    padding: 20px;
                }
            `}</style>

            <div className="left-side">
                <Select
                    value={currency}
                    onChange={(event) => onCurrencyChange(event.target.value)}
                />

                <Text size={Text.Sizes.SMALL} color={Text.Colors.WHITE_OPACITY}>
                    you have: {formatDecimals(pocketAmount)}
                </Text>
            </div>

            <div className="right-side">
                <Input
                    sign={sign}
                    value={formatDecimals(amount)}
                    onChange={(event) => onAmountChange(event.target.value)}
                />

                <Text size={Text.Sizes.SMALL} color={Text.Colors.WHITE_OPACITY}>
                    rate: {formatDecimals(rate) || 'Loading...'}
                </Text>
            </div>
        </div>
    );