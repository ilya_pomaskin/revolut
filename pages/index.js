import React, { useEffect } from 'react'
import Head from 'next/head'
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

import {
  setAmountEffect,
  setCurrencyEffect,
  exchangeEffect,
} from '../store/exchangeForm/actions';
import {
  selectAmount,
  selectSourceCurrency,
  selectDestinationAmount,
  selectDestinationCurrency,
  selectRate,
  selectBackRate,
  selectSourcePocket,
  selectDestinationPocket,
  selectError
} from '../store/exchangeForm/selectors';
import { startCurrencyPollEffect } from '../store/currency/actions';
import { CurrencyForm } from '../components/CurrencyForm';
import { Text } from '../components/Text';
import { Footer } from '../components/Footer';

export const Home = ({
  amount,
  sourceCurrency,
  destinationAmount,
  destinationCurrency,
  rate,
  backRate,
  sourcePocket,
  destinationPocket,
  error,
  setAmountEffect,
  setCurrencyEffect,
  exchangeEffect,
  startCurrencyPollEffect
}) => {
  useEffect(() => {
    startCurrencyPollEffect()
  }, []);

  return (
    <div>
      <Head>
        <title>Exchange</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <style global jsx>{`
        body {
          font-family: Helvetica, serif;
        }
      `}</style>
      <style jsx>{`
        .button {
          border: 1px solid white;
          padding: 10px;
          color: white;
          background: transparent;
          outline: none;
        }

        .button:active {
          border: 1px solid rgba(255, 255, 255, 0.6);
        }
      `}</style>

      <CurrencyForm
        isSource
        currency={sourceCurrency}
        sign="-"
        amount={amount}
        rate={rate}
        pocketAmount={sourcePocket}
        onCurrencyChange={(value) => setCurrencyEffect('source', value)}
        onAmountChange={(value) => setAmountEffect('source', value)}
      />

      <CurrencyForm
        currency={destinationCurrency}
        sign="+"
        amount={destinationAmount}
        rate={backRate}
        pocketAmount={destinationPocket}
        onCurrencyChange={(value) => setCurrencyEffect('destination', value)}
        onAmountChange={(value) => setAmountEffect('destination', value)}
      />

      <Footer>
        {error && <Text color={Text.Colors.RED}>
          Something went wrong: {error}
        </Text>}

        <button
          className="button"
          onClick={exchangeEffect}
        >
          Exchange
        </button>
      </Footer>
    </div>
  )
}

export default connect(createStructuredSelector({
  amount: selectAmount,
  sourceCurrency: selectSourceCurrency,
  destinationAmount: selectDestinationAmount,
  destinationCurrency: selectDestinationCurrency,
  rate: selectRate,
  backRate: selectBackRate,
  sourcePocket: selectSourcePocket,
  destinationPocket: selectDestinationPocket,
  error: selectError
}), {
  setAmountEffect,
  setCurrencyEffect,
  exchangeEffect,
  startCurrencyPollEffect
})(Home);
