import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { Home } from '../index';

describe('Index page', () => {
    it('renders correctly', () => {
        expect(
            renderer
                .create(
                    <Home
                        amount={123}
                        sourceCurrency='USD'
                        destinationAmount={333}
                        destinationCurrency='GBP'
                        rate={2.34}
                        rateError={null}
                        backRate={4.32}
                        sourcePocket={300}
                        destinationPocket={10}
                        error={null}
                        setAmountEffect={() => null}
                        setCurrencyEffect={() => null}
                        exchangeEffect={() => null}
                        startCurrencyPollEffect={() => null}
                    />
                )
                .toJSON()
        ).toMatchSnapshot();
    });

    it('renders errors', () => {
        expect(
            renderer
                .create(
                    <Home
                        amount={123}
                        sourceCurrency='USD'
                        destinationAmount={333}
                        destinationCurrency='GBP'
                        rate={2.34}
                        rateError='No rates'
                        backRate={4.32}
                        sourcePocket={300}
                        destinationPocket={10}
                        error='Validation failed'
                        setAmountEffect={() => null}
                        setCurrencyEffect={() => null}
                        exchangeEffect={() => null}
                        startCurrencyPollEffect={() => null}
                    />
                )
                .toJSON()
        ).toMatchSnapshot();
    });

    it('call startCurrencyPollEffect', () => {
        const mockStartCurrencyPollEffect = jest.fn();
        mount(
            <Home
                amount={123}
                sourceCurrency='USD'
                destinationAmount={333}
                destinationCurrency='GBP'
                rate={2.34}
                rateError='No rates'
                backRate={4.32}
                sourcePocket={300}
                destinationPocket={10}
                error='Validation failed'
                setAmountEffect={() => null}
                setCurrencyEffect={() => null}
                exchangeEffect={() => null}
                startCurrencyPollEffect={mockStartCurrencyPollEffect}
            />
        );

        expect(mockStartCurrencyPollEffect).toBeCalled();
    });
});