import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { createLogger } from 'redux-logger';

import reducer from './reducers.js';
import mySaga from './sagas/index.js';

const logger = createLogger({
    collapsed: true,
    diff: true
});

export default () => {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(
        reducer,
        applyMiddleware(sagaMiddleware, logger)
    );

    sagaMiddleware.run(mySaga);

    return store;
}

EUR: 0.9535196268077429
GBP: 0.8016464814463817
USD: 1