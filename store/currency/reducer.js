import { types } from './actions';

const initialState = {};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SET_CURRENCY:
            return {
                ...state,
                ...action.payload
            };
    }

    return state;
};