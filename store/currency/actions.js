export const types = {
    SET_CURRENCY: 'SET_CURRENCY',
    START_CURRENCY_POLL_EFFECT: 'START_CURRENCY_POLL_EFFECT',
};

export const startCurrencyPollEffect = (currency) => ({
    type: types.START_CURRENCY_POLL_EFFECT,
    payload: currency
});

export const setCurrency = (payload) => ({
    type: types.SET_CURRENCY,
    payload
});
