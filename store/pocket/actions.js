export const types = {
    SET_POCKET_VALUE: 'SET_POCKET_VALUE',
};

export const setPocketValue = (pocket, value) => ({
    type: types.SET_POCKET_VALUE,
    payload: {
        pocket,
        value
    }
});
