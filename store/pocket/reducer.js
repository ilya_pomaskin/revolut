import { types } from './actions';

const initialState = {
    USD: 50,
    EUR: 50,
    GBP: 0
};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SET_POCKET_VALUE:
            const { pocket, value } = action.payload;

            return {
                ...state,
                [pocket]: value
            };
    }

    return state;
};