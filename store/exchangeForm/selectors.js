import { createSelector } from 'reselect';
import { selectCurrencies } from '../currency/selectors';
import { selectPockets } from '../pocket/selectors';

export const selectAmount = (state) => state.exchangeForm.amount;
export const selectSourceCurrency = (state) => state.exchangeForm.source;
export const selectDestinationCurrency = (state) => state.exchangeForm.destination;
export const selectError = (state) => state.exchangeForm._error;

export const selectRates = createSelector(
    selectCurrencies,
    selectSourceCurrency,
    selectDestinationCurrency,
    (currencies, src, dst) => [currencies[dst], currencies[src]]
);

export const selectRate = createSelector(
    selectRates,
    ([srcRate, dstRate]) => Number(dstRate / srcRate) || 0
);

export const selectBackRate = createSelector(
    selectRates,
    ([srcRate, dstRate]) => Number(srcRate / dstRate) || 0
);

export const selectSourcePocket = createSelector(
    selectPockets,
    selectSourceCurrency,
    (pockets, src) => pockets[src]
);

export const selectDestinationPocket = createSelector(
    selectPockets,
    selectDestinationCurrency,
    (pockets, dst) => pockets[dst]
);

export const selectDestinationAmount = createSelector(
    selectAmount,
    selectRate,
    (amount, rate = 0) => amount * rate
);
