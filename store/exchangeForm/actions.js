export const types = {
    SET_AMOUNT_EFFECT: 'SET_AMOUNT_EFFECT',
    SET_CURRENCY_EFFECT: 'SET_CURRENCY_EFFECT',
    EXCHANGE_EFFECT: 'EXCHANGE_EFFECT',
    SET_FIELD: 'SET_FIELD'
};

export const setAmountEffect = (type, value) => ({
    type: types.SET_AMOUNT_EFFECT,
    payload: {
        type,
        value
    }
});

export const setCurrencyEffect = (type, value) => ({
    type: types.SET_CURRENCY_EFFECT,
    payload: {
        type,
        value
    }
})

export const exchangeEffect = () => ({
    type: types.EXCHANGE_EFFECT
});

export const setField = (field, value) => ({
    type: types.SET_FIELD,
    payload: {
        field,
        value
    }
});