import { types } from './actions';

const initialState = {
    amount: 0,
    source: 'USD',
    destination: 'GBP',
    _error: null
};

export default (state = initialState, action) => {
    switch (action.type) {
        case types.SET_FIELD:
            const { field, value } = action.payload;

            return {
                ...state,
                [field]: value
            };
    }

    return state;
};