import { combineReducers } from 'redux';
import pocket from './pocket/reducer';
import currency from './currency/reducer';
import exchangeForm from './exchangeForm/reducer';

export default combineReducers({
    pocket,
    currency,
    exchangeForm
});
