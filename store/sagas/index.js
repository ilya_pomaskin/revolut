import { all } from 'redux-saga/effects'
import { currencySaga } from './currency';
import { exchangeFormSaga } from './exchangeForm';

export default function* root() {
    yield all([
        currencySaga(),
        exchangeFormSaga()
    ]);
}