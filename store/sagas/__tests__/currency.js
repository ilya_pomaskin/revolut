import { expectSaga } from 'redux-saga-test-plan';
import * as matchers from 'redux-saga-test-plan/matchers';
import { currencyPollHandler } from '../currency';
import { setCurrency } from '../../currency/actions';
import * as api from '../../../modules/api';

describe('currency saga', () => {
    it('set rates', () => expectSaga(currencyPollHandler)
        .provide([
            [matchers.call.fn(api.mockGetCurrency), { rates: { EUR: 2 } }],
            [matchers.call.fn(api.getCurrency), { rates: { EUR: 2 } }],
        ])
        .put(
            setCurrency({
                EUR: 2
            })
        )
        .run()
    );
});
