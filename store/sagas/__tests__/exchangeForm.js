import { expectSaga } from 'redux-saga-test-plan';
import { exchangeHandler, setAmountHandler, setCurrencyHandler } from '../exchangeForm';
import { setAmountEffect, setCurrencyEffect, setField } from '../../exchangeForm/actions';
import { setPocketValue } from '../../pocket/actions';

describe('exchangeForm saga', () => {
    describe('set amount', () => {
        it('validate value', () =>
            expectSaga(setAmountHandler, setAmountEffect('source', '1'))
                .withState({
                    pocket: {
                        USD: 100,
                        GBP: 0
                    },
                    exchangeForm: {
                        source: 'USD',
                        destination: 'GBP'
                    },
                    currency: {
                        USD: 1,
                        GBP: 5
                    }
                })
                .not
                .put(setField('amount', '1'))
                .run())
        it('check rates', () =>
            expectSaga(setAmountHandler, setAmountEffect('source', 10))
                .withState({
                    pocket: {
                        USD: 100,
                        GBP: 0
                    },
                    exchangeForm: {
                        source: 'USD',
                        destination: 'GBP'
                    },
                    currency: {}
                })
                .not
                .put(setField(10))
                .run())
        it('check money', () =>
            expectSaga(setAmountHandler, setAmountEffect('source', 5))
                .withState({
                    pocket: {
                        USD: 3,
                        GBP: 0
                    },
                    exchangeForm: {
                        source: 'USD',
                        destination: 'GBP'
                    },
                    currency: {
                        USD: 1,
                        GBP: 1
                    }
                })
                .not
                .put(setField('amount', 5))
                .run())
        it('set amount', () => {
            expectSaga(setAmountHandler, setAmountEffect('source', 5))
                .withState({
                    pocket: {
                        USD: 10,
                        GBP: 0
                    },
                    exchangeForm: {
                        source: 'USD',
                        destination: 'GBP'
                    },
                    currency: {
                        USD: 1,
                        GBP: 1
                    }
                })
                .put(setField('amount', 5))
                .run()
        })
    });

    describe('set currency', () => {
        it('set new currency', () =>
            expectSaga(setCurrencyHandler, setCurrencyEffect('destination', 'GBP'))
                .withState({
                    pocket: {
                        USD: 3,
                        GBP: 0
                    },
                    exchangeForm: {
                        source: '',
                        destination: ''
                    },
                    currency: {}
                })
                .put(setField('destination', 'GBP'))
                .run())
    });

    describe('exchange', () => {
        it('validate amount', () =>
            expectSaga(exchangeHandler)
                .withState({
                    pocket: {
                        USD: 3,
                        GBP: 0
                    },
                    exchangeForm: {
                        amount: 0.0001,
                        source: 'USD',
                        destination: 'GBP'
                    },
                    currency: {}
                })
                .put(setField('_error', 'Invalid amount'))
                .run());
        it('validate money', () =>
            expectSaga(exchangeHandler)
                .withState({
                    pocket: {
                        USD: 3,
                        GBP: 0
                    },
                    exchangeForm: {
                        amount: 5,
                        source: 'USD',
                        destination: 'GBP'
                    },
                    currency: {}
                })
                .put(setField('_error', 'Not enough money'))
                .run());
        it('exchange', () =>
            expectSaga(exchangeHandler)
                .withState({
                    pocket: {
                        USD: 3,
                        GBP: 0
                    },
                    exchangeForm: {
                        amount: 3,
                        source: 'USD',
                        destination: 'GBP'
                    },
                    currency: {
                        USD: 1,
                        GBP: 0.5
                    }
                })
                .put(setField('_error', null))
                .put(setPocketValue('USD', 0))
                .put(setPocketValue('GBP', 6))
                .put(setField('amount', 0))
                .run());
    });
});