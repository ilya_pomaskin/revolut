import { put, takeLatest, delay, call } from 'redux-saga/effects'
import * as api from '../../modules/api';
import { types, setCurrency } from '../currency/actions';

export function* currencyPollHandler() {
    while (true) {
        const response = yield call(api.mockGetCurrency);
        yield put(setCurrency(response.rates));
        yield delay(100000);
    }
}

export function* currencySaga() {
    yield takeLatest(
        types.START_CURRENCY_POLL_EFFECT,
        currencyPollHandler
    );
}