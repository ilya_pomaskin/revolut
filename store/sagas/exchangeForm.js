import { put, select, takeEvery } from 'redux-saga/effects'
import { types, setField } from '../exchangeForm/actions';
import {
    selectAmount,
    selectSourceCurrency,
    selectDestinationCurrency,
    selectSourcePocket,
    selectBackRate,
    selectDestinationAmount
} from '../exchangeForm/selectors';
import { selectPockets } from '../pocket/selectors';
import { setPocketValue } from '../pocket/actions';

function isAmountValid(amount) {
    const isNumber = !isNaN(amount);
    const isNegative = amount < 0;
    const [, digitsAfterDot] = String(amount).split('.');
    const hasLessThanTwoDigits = !digitsAfterDot || digitsAfterDot && digitsAfterDot.length <= 2;

    return isNumber && !isNegative && hasLessThanTwoDigits;
}

export function* setAmountHandler(action) {
    const { value, type } = action.payload;
    const nextValue = Number(value);
    const backRate = yield select(selectBackRate);
    const hasRates = !isNaN(backRate);
    const nextAmount = type === 'source'
        ? nextValue
        : Number((nextValue * backRate).toFixed(2));
    const moneyInPocket = yield select(selectSourcePocket);
    const hasEnoughMoney = nextAmount <= moneyInPocket;

    if (!isAmountValid(nextValue) || !hasRates || !hasEnoughMoney) {
        return;
    }

    yield put(setField('amount', nextAmount));
}

export function* setCurrencyHandler(action) {
    const { type, value } = action.payload;
    const sourceValue = yield select(selectSourceCurrency);
    const isSourceValueChanged = type === 'source' && value !== sourceValue;

    if (isSourceValueChanged) {
        yield put(setField('amount', 0));
    }

    yield put(setField(type, value));
}

export function* exchangeHandler() {
    const amount = yield select(selectAmount);
    const destinationAmount = yield select(selectDestinationAmount);
    const src = yield select(selectSourceCurrency);
    const dst = yield select(selectDestinationCurrency);
    const pockets = yield select(selectPockets);

    if (isNaN(amount) || amount < 0.01) {
        yield put(setField('_error', 'Invalid amount'));
        return;
    }

    if (pockets[src] < amount) {
        yield put(setField('_error', 'Not enough money'));
        return;
    }

    yield put(setField('_error', null));
    yield put(setPocketValue(src, pockets[src] - amount));
    yield put(setPocketValue(dst, pockets[dst] + destinationAmount));
    yield put(setField('amount', 0));
}

export function* exchangeFormSaga() {
    yield takeEvery(types.SET_AMOUNT_EFFECT, setAmountHandler);
    yield takeEvery(types.SET_CURRENCY_EFFECT, setCurrencyHandler)
    yield takeEvery(types.EXCHANGE_EFFECT, exchangeHandler);
}